extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro2::TokenTree;
use syn::{parse_macro_input, DeriveInput };
use quote::quote;

fn unwrap_option(ty: &syn::Type) -> Option<&syn::Type> {
    if let syn::Type::Path(ref typepath) = ty {
        let segments = &typepath.path.segments;
        if !(segments.len() == 1 && segments[0].ident == "Option") {
            return None
        }
        if let syn::PathArguments::AngleBracketed(syn::AngleBracketedGenericArguments { ref args, .. }) = segments[0].arguments {
            if let syn::GenericArgument::Type(ref wrappedtype) = args[0] { // we are sure that options has one argument
                return Some(&wrappedtype)
            }
        }
    }
    None
}

#[proc_macro_derive(Builder, attributes(builder))]
pub fn derive(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = parse_macro_input!(input as DeriveInput);
    println!("{:#?}", ast); // print the abstract syntax tree (ast)
    let name = &ast.ident;
    // reading the names fields out of the ast of the detected struct
    let fields = if let syn::Data::Struct(syn::DataStruct { fields: syn::Fields::Named(syn::FieldsNamed {ref named, ..} ), ..}) = ast.data {
      named
    } else {
        unimplemented!()
    };
    let field_is_option_type = |field: &syn::Field| {
        if let syn::Type::Path(ref typepath) = field.ty {
            return typepath.path.segments.len() == 1 &&
                typepath.path.segments[0].ident == "Option"
        }
        false
    };

    // reads ast struct fields and generates Option<type> fields for builder with same names
    let option_fields = fields.iter().map( |field| {
        let field_name = &field.ident;
        let ty = &field.ty;
        if unwrap_option(ty).is_some() {
            quote! { #field_name: #ty }
        } else {
            quote! { #field_name: std::option::Option<#ty> }
        }
    });
    //
    let extended_methods = fields.iter().filter_map( |field| {
        for attribute in &field.attrs {
            let segments = &attribute.path.segments;
            if segments.len() == 1 && segments[0].ident == "builder" {
                if let Some(TokenTree::Group(g)) = attribute.tokens.clone().into_iter().next() {
                    let mut tokens = g.stream().into_iter();
                    match tokens.next().unwrap() {
                        TokenTree::Ident(ref ident) => assert_eq!("each", ident.to_string()),
                        token_tree => panic!("expected 'each' found: {}", token_tree)
                    };
                    match tokens.next().unwrap() {
                        TokenTree::Punct(ref punct) => assert_eq!('=', punct.as_char()),
                        token_tree => panic!("expected '=' found: {}", token_tree)
                    };
                    let arg = tokens.next().unwrap();
                    let arg = syn::Ident::new(&format!("{}", arg), arg.span());
                    return Some(quote! { fn #arg() {} });
                 }
            }
        }
        None
    });
    // builds Some(Type) setter methods from ast struct fields
    let setter_methods = fields.iter().map( |field| {
        let field_name = &field.ident;
        let ty = &field.ty;
        if let Some(inner_type) = unwrap_option(ty) {
            quote! {
               pub fn #field_name(&mut self, #field_name: #inner_type) -> &mut Self {
                    self.#field_name = Some(#field_name);
                    self
                }
            }
        } else {
            quote! {
               pub fn #field_name(&mut self, #field_name: #ty) -> &mut Self {
                    self.#field_name = Some(#field_name);
                    self
                }
            }
        }
    });
    // builds lines for builder to instantiate original struct from ast struct fields
    let build_fields = fields.iter().map(|field| {
        let field_name = &field.ident;
        if field_is_option_type(&field) {
            quote! {
                #field_name: self.#field_name.clone()
            }
        } else {
            quote! {
                #field_name: self.#field_name.clone().ok_or(concat!(stringify!(#field_name), "is not set"))?
            }
        }
    });
    // using the fields to add default values for the builder
    let default_values = fields.iter().map( |field| {
        let field_name = &field.ident;
        quote! { #field_name: None }
    });
    let builder_name = format!("{}Builder", name);
    let builder_ident = syn::Ident::new(&builder_name, name.span());
    // using the iterators generated above to make a simple struct builder
    let quote_tokens = quote! {
        pub struct #builder_ident {
           #(#option_fields,)*
        }
        impl #builder_ident {
            #(#setter_methods)*
            #(#extended_methods)*
            pub fn build(&self) -> Result<#name, Box<dyn std::error::Error>> {
               Ok(#name {
                    #(#build_fields,)*
               })
            }
        }
        impl #name {
            fn builder() -> #builder_ident {
                #builder_ident {
                    #(#default_values,)*
                }
            }
        }
    };
    quote_tokens.into()
}